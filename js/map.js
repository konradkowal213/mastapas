var tapas = [];
/*
* PRUEBAS
*
* var BASE="http://mastapas.hol.es/";
 var BASEURL="http://mastapas.hol.es/api/";
* */

$.getScript('js/BD.js', function() {});

function getPoints() {
	$.ajax({
		type: 'GET',
		contentType: 'application/json',
        url: BASEURL + 'tapas',
		success: function(data){
			var array = JSON.parse(data);
			for(i=0;i<array.length;i++){
				var valueToPush = new Array();
				valueToPush[0] = array[i].Calle.valueOf();
				valueToPush[1] = parseFloat(array[i].Latitud);
				valueToPush[2] = parseFloat(array[i].Longitud);
				valueToPush[3] = array[i].Nombre.valueOf();
				valueToPush[4] = parseInt(array[i].Puntuacion);
				valueToPush[5] = array[i].Descripcion.valueOf();
                valueToPush[6] = parseInt(array[i].ID_tapa);
				tapas.push(valueToPush);
			}
			initMap();
		}
	});
}

function initMap() {
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 11,
		center: {lat: 40.417746, lng: -3.703366}
	});
	setMarkers(map);
}
	
function setMarkers(map) {
	var infoWindow = new google.maps.InfoWindow();
	var image = 'img/marker.png';
	for (var i = 0; i < tapas.length; i++) {
		var tapa = tapas[i];
        var marker = new google.maps.Marker({
            position: {lat: tapa[1], lng: tapa[2]},
            map: map,
			icon:image,
            title: tapa[0]
        });
        (function (marker, tapa) {
            google.maps.event.addListener(marker, "click", function (e) {
				infoWindow.setContent('' +
					'<h2 class="infowindow-title">'+tapa[3]+'</h2> ' +
						'<div class="slider">'+
							'<div class="slide_viewer">'+
								'<div class="slide_group">'+
									'<div class="slide">'+
										'<p>'+tapa[5]+'</p>'+
                                        '<p hidden id="tapaId">'+tapa[6]+'</p>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'<p class="getDesc" style="font-size: 15px; max-width: 400px;"><a style="color:red; font-size: 15px; font-weight: bold;">VER MAS DESCRIPCIONES</a></p>' +
                    '<p class="addDesc" style="font-size: 15px; max-width: 400px;"><a style="color:red; font-size: 15px; font-weight: bold;">AÑADIR DESCRIPCION</a></p>' +
					' </span>'	)
				infoWindow.open(map, marker);
            });
        })(marker, tapa);
	}
}

$(document).on("click", ".getDesc", function(){
    var idTapa=document.getElementById("tapaId").innerHTML;
    window.location=BASE+"/comentarios.php?id="+idTapa;
});
$(document).on("click", ".addDesc", function(){
    var idTapa=document.getElementById("tapaId").innerHTML;
    window.location=BASE+"/newDescription.php?id="+idTapa;
});

	
	
