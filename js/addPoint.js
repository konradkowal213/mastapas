$.getScript('js/BD.js', function() {});
//var BASE="http://mastapas.hol.es/";
//var BASEURL="http://mastapas.hol.es/api/";
function loadScript(){
	//$('#mostrar').hide();
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 11,
		center: {lat: 40.417746, lng: -3.703366}
	});
	var geocoder = new google.maps.Geocoder;
	var infowindow = new google.maps.InfoWindow;
	var input = document.getElementById('nombreStablecimiento');
	var searchBox = new google.maps.places.SearchBox(input);
	var marker;
	var markers = [];
	searchBox.addListener('places_changed', function() {
		var places = searchBox.getPlaces();
	
		if (places.length == 0) {
			return;
		}
	
		// Clear out the old markers.
		markers.forEach(function(marker) {
			marker.setMap(null);
		});

	
		// For each place, get the icon, name and location.
		var bounds = new google.maps.LatLngBounds();
		places.forEach(function(place) {
			if (!place.geometry) {
				console.log("Returned place contains no geometry");
				return;
			}
			var icon = {
				url: place.icon,
				size: new google.maps.Size(71, 71),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(17, 34),
				scaledSize: new google.maps.Size(25, 25)
			};
			// Create a marker for each place.
			markers.push(new google.maps.Marker({
				map: map,
				icon: icon,
				title: place.name,
				position: place.geometry.location,
				draggable:true,
			}));
			geocodeLatLng(geocoder ,markers[markers.length-1].getPosition().lat(),markers[markers.length-1].getPosition().lng() );
			google.maps.event.addListener(markers[markers.length-1], 'dragend', function(){
				geocodeLatLng(geocoder ,markers[markers.length-1].getPosition().lat(),markers[markers.length-1].getPosition().lng() );
			});
			if (place.geometry.viewport) {
				// Only geocodes have viewport.
				bounds.union(place.geometry.viewport);
			} else {
				bounds.extend(place.geometry.location);
			}
		});
		map.fitBounds(bounds);


	});
	function geocodeLatLng(geocoder, latitud,longitud) {
		var latlng = {lat: parseFloat(latitud), lng: parseFloat(longitud)};
		geocoder.geocode({'location': latlng}, function(results, status) {
			if (status === 'OK') {
				if (results[0]) {
					document.getElementById("streetnumber").value=results[0].formatted_address;
					document.getElementById("lat").value=latlng.lat;
                    document.getElementById("long").value=latlng.lng;
					console.log(results[0].formatted_address);

				} else {
					window.alert('No results found');
				}
			} else {
				window.alert('Geocoder failed due to: ' + status);
			}
		});
	}


}


var loadFile = function(event) {

};
