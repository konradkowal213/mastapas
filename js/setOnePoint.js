var tapas = [];
//$.getScript('js/jsonTransform.js', function() {});

/*var BASE="http://mastapas.hol.es/";
var BASEURL="http://mastapas.hol.es/api/";*/
$.getScript('js/jsonTransform.js', function() {});
function getOnePoint(idTapa) {
	$.ajax({
		type: 'GET',
		contentType: 'application/json',
        url: BASEURL + 'tapas/'+idTapa,
		success: function(data){
			var array = JSON.parse(data);
            $('.panel-heading').append('<strong>'+array[0].Nombre+'</strong><em>('+array[0].Calle+')</em>');
            var valueToPush = new Array();
			valueToPush[0] = parseFloat(array[0].Latitud);
			valueToPush[1] = parseFloat(array[0].Longitud);
			tapas.push(valueToPush);
			initMap();
		}
	});
}
function validarDescr() {
    var desc= $("#desc").val();
    var precio = $("#precio").val();
	var puntu=$("#punctuation").val();
    var im1=$('#img1').prop('src');
    var im2=$('#img2').prop('src');
    var im3=$('#img3').prop('src');

    if(desc       == ""){
        $("#descrip").addClass('has-error');
        return false;
    }else if(precio==""){
        $("#descrip").removeClass('has-error');
        $("#price").addClass('has-error');
        return false;
	}else if(im1=="" && im2=="" && im3==""){
        $("#descrip").removeClass('has-error');
        $("#price").removeClass('has-error');
        $("#points").removeClass('has-error');
		alert("al menos 1");
		return false;
	}else {

        alert("Enviando datos, por favor espere.");
	    var formData = JSON.stringify($(".login-form").serializeJSON());
        formData["img1"] = $('#img1').prop('src');
        formData["img2"] = $('#img2').prop('src');
        formData["img3"] = $('#img3').prop('src');
        var slice2 = formData.slice(0, -1);

        slice2 = slice2 + ',"img1":"' + $('#img1').prop('src') + '",';
        slice2 = slice2.slice(0, -1);

        slice2 = slice2 + ',"img2":"' + $('#img2').prop('src') + '",';
        slice2 = slice2.slice(0, -1);

        slice2 = slice2 + ',"img3":"' + $('#img3').prop('src') + '"}';

         $.ajax({
             type: "POST",
             url: BASEURL+'newComment',
             data: slice2,
             dataType: "json",
             contentType : "application/json",
             success: function() {
                 alert("Se ha subido con exito, ahora el admin tiene que confirmar lo subido.");
                 window.location = BASE+"/map.php";
             },
             error: function(data) {
                alert("Algo ha salido mal, pruebe de nuevo");
             }
         });
    }


}


function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#img1')
                .attr('src', e.target.result)
                .width(502)
                .height(319);
        };
        reader.readAsDataURL(input.files[0]);
    }else{
        $('#img1').removeAttr('src');
	}
}
function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#img2')
                .attr('src', e.target.result)
                .width(502)
                .height(319);
        };
        reader.readAsDataURL(input.files[0]);
    }else{
        $('#img2').removeAttr('src', "e");
    }
}

function readURL3(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#img3')
                .attr('src', e.target.result)
                .width(502)
                .height(319);
        };
        reader.readAsDataURL(input.files[0]);
    }else{
        $('#img3').removeAttr('src', "e");
    }
}
function validarPuntuacion() {

}
function initMap() {
	tapa=tapas[0];
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 14,
		center: {lat: tapa[0], lng: tapa[1]}
	});
	setMarkers(map);
}
	
function setMarkers(map) {
	var infoWindow = new google.maps.InfoWindow();
	var image = 'img/marker.png';
	for (var i = 0; i < tapas.length; i++) {
		var tapa = tapas[i];
        var marker = new google.maps.Marker({
            position: {lat: tapa[0], lng: tapa[1]},
            map: map,
			icon:image
        });
	}
}

	
	
