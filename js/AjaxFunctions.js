/**
 * Created by konra on 30/04/2017.
 */
/**
 * PRUEBAS
 *
 *
 var BASE="http://mastapas.hol.es/";
 var BASEURL="http://mastapas.hol.es/api/";
 *
 * Created by konra on 29/04/2017.
 */

$.getScript('js/jsonTransform.js', function() {});
var BASEURL="http://localhost/plantillamastapas/api/";
var BASE="http://localhost/plantillamastapas";
var currentTD;

function getDescription(idTapa) {

    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: BASEURL + 'description/'+idTapa,
        beforeSend:function () {

        },
        complete:function () {

        },
        success: function(data){
            var array = JSON.parse(data);
           $('.panel-heading').append('<strong>'+array[0].Tapa[0].Nombre+'</strong><em>('+array[0].Tapa[0].Calle+')</em>');
            for (i = 0; i < array.length; i++) {
                $('.bodyTable').append('<tr id="comen'+ i+'" >');
                currentTD=document.getElementById("comen"+i);
                $('#comen'+i).append("<td>"+array[i].Descripcion+"</td>");
                $('#comen'+i).append("<td>"+array[i].Fecha_publicacion+"</td>");
                $('#comen'+i).append("<td>"+array[i].Precio+" €</td>");
                $('#comen'+i).append("<td>"+array[i].Puntuacion+" puntos</td>");
                $('#comen'+i).append("<td> <button type='submit' onclick='imprimir(this)' class='btn btn-primary center-block'>Ver fotos<p hidden id='idDescripcion'>"+array[i].ID_descripcion+"</p></button> </td>");
                $('.bodyTable').append("</tr>");
            }
        }
    });
}

function getMisPublicaciones(idTapa) {

    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: BASEURL + 'descriptionUser/'+idTapa,
        beforeSend:function () {

        },
        complete:function () {

        },
        success: function(data){
            var array = JSON.parse(data);
            console.log(array.length);
            $('.panel-heading').append('<strong>Mis publicaciones</strong><em></em>');

            if(array.length!=0){
                for (i = 0; i < array.length; i++) {
                    $('.bodyTable').append('<tr id="comen'+ i+'" >');
                    currentTD=document.getElementById("comen"+i);
                    $('#comen'+i).append("<td>"+array[i].Descripcion+"</td>");
                    $('#comen'+i).append("<td>"+array[i].Fecha_publicacion+"</td>");
                    $('#comen'+i).append("<td>"+array[i].Precio+" €</td>");
                    $('#comen'+i).append("<td>"+array[i].Puntuacion+" puntos</td>");
                    if(array[i].Confirmado==1){
                        $('#comen'+i).append("<td>Aceptado</td>");
                    }else if(array[i].Confirmado==0){
                        $('#comen'+i).append("<td>Denegado</td>");
                    }else{
                        $('#comen'+i).append("<td>Pendiente de revisión</td>");
                    }
                    $('#comen'+i).append("<td> <button type='submit' onclick='imprimir(this)' class='btn btn-primary center-block'>Ver fotos<p hidden id='idDescripcion'>"+array[i].ID_descripcion+"</p></button> </td>");
                    $('.bodyTable').append("</tr>");
                }
            }else{
                $('#tablaMisPublicaciones').remove();
                $('.panel-body').append('<h3 class="text-center">Aún no has hecho ninguna publicación</h3>')
                console.log("vacio")
            }

            /* for (i = 0; i < array.length; i++) {
             $('.bodyTable').append('<div id="comen'+i+'" class="col-lg-3 col-md-4 col-sm-6"></div>');
             currentDiv=document.getElementById("comen"+i);
             if(array[i].Imagenes.length==0){
             var DOM_img = document.createElement("img");
             DOM_img.src = 'img/no-image.png';
             DOM_img.style.height='200px';
             DOM_img.style.width='230px';
             currentDiv.appendChild(DOM_img);
             }else{
             for(f=0;f<array[i].Imagenes.length;f++){

             if(f==0){
             var aClass=document.createElement('a');
             aClass.className='venobox_custom';
             aClass.setAttribute('data-gall','gall'+i);
             aClass.href= array[i].Imagenes[f].base64_img;
             var DOM_img = document.createElement("img");
             DOM_img.src = array[i].Imagenes[f].base64_img;
             DOM_img.style.height='200px';
             DOM_img.style.width='230px';
             aClass.appendChild(DOM_img)
             currentDiv.appendChild(aClass);
             }else{
             var aClass=document.createElement('a');
             aClass.className='venobox_custom';
             aClass.setAttribute('data-gall','gall'+i);
             aClass.href= array[i].Imagenes[f].base64_img;
             var DOM_img = document.createElement("img");
             DOM_img.src = array[i].Imagenes[f].base64_img;
             DOM_img.style.visibility='hidden';
             DOM_img.style.height='0px';
             DOM_img.style.width='0px';
             aClass.appendChild(DOM_img)
             currentDiv.appendChild(aClass);
             }

             }
             }
             var pre= document.createElement('pre');
             pre.appendChild(document.createTextNode(array[i].Descripcion));
             currentDiv.appendChild(pre);
             $(".venobox_custom").venobox({
             ramewidth: '400px',
             frameheight: '800px',
             titleattr: 'data-title',
             numeratio: true,            // default: false
             infinigall: true            // default: false
             });
             }*/

        }
    });
}

function imprimir(elemento){
    var id=jQuery(elemento).children("p")[0].innerText;
    window.location.replace("publicacion.php?id="+id);

}

function closeSession(){
    $.ajax({
        type: "GET",
        url: BASEURL+'logout',
        success: function() {
            window.location.replace("index.php");
        },
        error: function(data) {
        }
    });
}

function validar() {



    var establecimiento = $("#nombreStablecimiento").val();
    var street = $("#streetnumber").val();
    var categoria= $("#categorias").val()
    var precio = $("#precio").val();
    var desc= $("#desc").val();
    var puntu=$("#punctuation").val();
    var lat=$("#lat").val();
    var long=$("#long").val();
    var im1=$('#img1').prop('src');
    var im2=$('#img2').prop('src');
    var im3=$('#img3').prop('src');

    if (establecimiento == "") {
        $("#estab").addClass('has-error');
        return false;
    }else if(street     == ""){
        $("#calle").addClass('has-error');
        return false;
    }else if(precio     == ""){
        $("#estab").removeClass('has-error');
        $("#calle").removeClass('has-error');
        $("#cat").removeClass('has-error');
        $("#price").addClass('has-error');
        return false;
    }else if(puntu       == ""){
        $("#estab").removeClass('has-error');
        $("#calle").removeClass('has-error');
        $("#cat").removeClass('has-error');
        $("#price").removeClass('has-error');
        $("#points").addClass('has-error');
        return false;
    }else if(desc       == ""){
        $("#estab").removeClass('has-error');
        $("#calle").removeClass('has-error');
        $("#cat").removeClass('has-error');
        $("#price").removeClass('has-error');
        $("#points").removeClass('has-error');
        $("#descrip").addClass('has-error');
        return false;
    }else if(im1=="" && im2=="" && im3==""){
        $("#descrip").removeClass('has-error');
        $("#price").removeClass('has-error');
        $("#points").removeClass('has-error');
        $("#punctuation").removeClass('has-error');
        alert("Tienes que subir al menos una foto");
        return false;
    }else {
        var formData = JSON.stringify($(".login-form").serializeJSON());
        console.log(formData);
        formData["img1"] = $('#img1').prop('src');
        formData["img2"] = $('#img2').prop('src');
        formData["img3"] = $('#img3').prop('src');
        var slice2 = formData.slice(0, -1);

        slice2 = slice2 + ',"img1":"' + $('#img1').prop('src') + '",';
        slice2 = slice2.slice(0, -1);

        slice2 = slice2 + ',"img2":"' + $('#img2').prop('src') + '",';
        slice2 = slice2.slice(0, -1);

        slice2 = slice2 + ',"img3":"' + $('#img3').prop('src') + '"}';
        $.ajax({
            type: "POST",
            url: BASEURL+'tapa',
            data: slice2,
            dataType: "json",
            contentType : "application/json",
            success: function() {
                alert("Se ha subido con exito, ahora el admin tiene que confirmar lo subido.");
                window.location = BASE+"/map.php";
            },
            error: function(data) {
                alert("Ha habido algun problema");
            }
        });
    }
}

function changePass() {
    var correo= $("#mail").val();
    var nick=$("#nick").val();
    console.log(correo);
    console.log(nick);
    var formData = JSON.stringify($(".lostPass-form").serializeJSON());
    $.ajax({
        type: "POST",
        url: BASEURL+'changePassword',
        data: formData,
        dataType: "json",
        contentType : "application/json",
        success: function() {
            alert("Gracias, tu contraseña ha sido cambiada, cerraremos la sesion por motivos de seguridad. Gracias");
            closeSession();
        },
        error: function(data) {
            alert("Ha habido algun problema");
        }
    });

}

function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#img1')
                .attr('src', e.target.result)
                .width(502)
                .height(319);
        };
        console.log(input.files[0].size);
        reader.readAsDataURL(input.files[0]);
    }else{
        $('#img1').removeAttr('src');
    }
}

function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#img2')
                .attr('src', e.target.result)
                .width(502)
                .height(319);
        };
        reader.readAsDataURL(input.files[0]);
    }else{
        $('#img2').removeAttr('src', "e");
    }
}

function readURL3(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#img3')
                .attr('src', e.target.result)
                .width(502)
                .height(319);
        };
        reader.readAsDataURL(input.files[0]);
    }else{
        $('#img3').removeAttr('src', "e");
    }
}

function registro() {
    var nombre = $("#name").val();
    var apellido = $("#lastName").val();
    var contrasena1= $("#pass1").val()
    var contrasena2 = $("#pass2").val();
    var correo= $("#mail").val();
    var nick=$("#nick").val();
    validacion_email = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    validacionContraseña = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/

    if(nombre   ==   ""){
        $("#divName").addClass('has-error');
        $("#errores").empty();
        $("#errores").append('<p>El nombre no puede estar vacio</p>');
        return false;
    }else if(apellido   ==   ""){
        $("#divName").removeClass('has-error');
        $("#divLastName").addClass('has-error');
        $("#errores").empty();
        $("#errores").append('<p>El apellido no puede estar vacio</p>');
        return false;
    }else if(contrasena1 ==   "" || contrasena1.length<6){
        $("#divName").removeClass('has-error');
        $("#divLastName").removeClass('has-error');
        $("#divPass1").addClass('has-error');
        $("#errores").empty();
        $("#errores").append("<p id='errorMail'>Error en la contraseña, debe cumplir los siguientes requisitos:</p>");
        $("#errores").append("<ul>");
        $("#errores").append("<li>Debe tener una longitud de como minimo 6 caracteres</li>");
        $("#errores").append("</ul>");
        $("#errores").append("<br>")
        return false;
    }else if(contrasena2 == ""){
        $("#divName").removeClass('has-error');
        $("#divLastName").removeClass('has-error');
        $("#divPass1").removeClass('has-error');
        $("#divPass2").addClass('has-error');
        $("#errores").empty();
        $("#errores").append('<p>La contraseña no puede estar vacia</p>');
        return false;
    }else if(contrasena1 != contrasena2){
        $("#divName").removeClass('has-error');
        $("#divLastName").removeClass('has-error');
        $("#divPass1").removeClass('has-error');
        $("#divPass2").removeClass('has-error');
        $("#divPass1").addClass('has-error');
        $("#divPass2").addClass('has-error');
        $("#errores").empty();
        $("#errores").append('<p>Las contraseñas no coinciden</p>');
        return false;
    }else if(correo == "" || !validacion_email.test(correo)){
        $("#divName").removeClass('has-error');
        $("#divLastName").removeClass('has-error');
        $("#divPass1").removeClass('has-error');
        $("#divPass2").removeClass('has-error');
        $("#divMail").addClass('has-error');
        $("#errores").empty();
        $("#errores").append('<p>No es una direccion de correo valida</p>');
        return false;
    }else if(nick == ""){
        $("#divName").removeClass('has-error');
        $("#divLastName").removeClass('has-error');
        $("#divPass1").removeClass('has-error');
        $("#divPass2").removeClass('has-error');
        $("#divMail").removeClass('has-error');
        $("#divNick").addClass('has-error');
        $("#errores").empty();
        $("#errores").append('<p>El nick no puede estar vacio</p>');
        return false;
    }else{
        $("#errores").empty();
        $("#divName").removeClass('has-error');
        $("#divLastName").removeClass('has-error');
        $("#divPass1").removeClass('has-error');
        $("#divPass2").removeClass('has-error');
        $("#divMail").removeClass('has-error');
        $("#divNick").removeClass('has-error');
        var formData = JSON.stringify($(".login-form").serializeJSON());
        $.ajax({
            type: "POST",
            url: BASEURL+'newUser',
            data: formData,
            dataType: "json",
            contentType : "application/json",
            success: function() {
                alert("Registro con exito");
                window.location = BASE+"/index.php";
            },
            error: function(data) {
               if(data.status==501){
                   alert("Ya existe alguien con ese nick o ese correo");
               }else {
                   alert("Error del servidor, intentelo de nuevo mas tarde");
               }
            }
        });

    }
}

function loadCategories() {
    var valorOption;
    var selectO = document.getElementById('categorias');
    opt = document.createElement("option");
    opt.value = "";
    opt.text = "";
    selectO.add(opt, 0);
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: BASEURL+'categorias',
        success: function (data) {
            var array = JSON.parse(data);
            for (i = 0; i < array.length; i++) {
                opt = document.createElement("option");
                opt.value = array[i].NombreCat.valueOf();
                opt.text = array[i].NombreCat.valueOf();
                selectO.add(opt, selectO.options[i + 1]);
            }
        }
    });
}

function login(){
    var formData = JSON.stringify($(".login-form").serializeJSON());
    $.ajax({
        type: "POST",
        url: BASEURL+'login',
        data: formData,
        dataType: "json",
        contentType : "application/json",
        success: function() {
            window.location.replace("map.php");
        },
        error: function(data) {
            window.location.replace("index.php?error=login");
        }
    });
}

function newPass(){
    var formData = JSON.stringify($(".newPass-form").serializeJSON());
    console.log(formData);
    $.ajax({
        type: "POST",
        url: BASEURL+'lostPassword',
        data: formData,
        dataType: "json",
        contentType : "application/json",
        success: function() {
            alert("Si has introducido los datos correctamente, hemos enviado una nueva contraseña a tu correo, si no esta en la bandeja de entrada, revisa la carpeta de spam");
            window.location.replace("index.php");
        },
        error: function(data) {
            alert("Si has introducido los datos correctamente, hemos enviado una nueva contraseña a tu correo, si no esta en la bandeja de entrada, revisa la carpeta de spam");
            window.location.replace("index.php");
        }
    });
}

function getPublicacion(tapaID){
    console.log(tapaID);
    $.ajax({
        type: 'GET',
        contentType: 'application/json',
        url: BASEURL + 'descriptionWEB/'+tapaID,
        beforeSend:function () {

        },
        complete:function () {

        },
        success: function(data){
            var array = JSON.parse(data);
            console.log(array);
            $('.panel-heading').append('<strong>'+array[0].Tapa[0].Nombre+'</strong><em></em>');

            for (i = 0; i < array.length; i++) {
                $('.bodyTable').append('<tr id="comen'+ i+'" >');
                currentTD=document.getElementById("comen"+i);
                $('#comen'+i).append("<td>"+array[i].Descripcion+"</td>");
                $('#comen'+i).append("<td>"+array[i].Fecha_publicacion+"</td>");
                $('#comen'+i).append("<td>"+array[i].Precio+" €</td>");
                $('#comen'+i).append("<td>"+array[i].Puntuacion+" puntos</td>");
                $('.bodyTable').append("</tr>");
            }
            if(array[0].Imagenes.length==1){
                console.log("una");
                $('#colImages').append('<div class="col-md-3"></div>')
                $('#colImages').append('<div class="col-md-6">' +
                    '<img id="theImg" style="max-width:100%;max-height:100%;" src="'+array[0].Imagenes[0].base64_img+'" />' +
                    '</div>')
                $('#colImages').append('<div class="col-md-3"></div>')
            }else if(array[0].Imagenes.length==2){
                console.log("dos");
                //$('#theDiv').prepend('<img id="theImg" src="theImg.png" />')

                $('#colImages').append('<div class="col-md-2"></div>')
                $('#colImages').append('<div class="col-md-3">' +
                    '<img id="theImg" style="max-width:100%;max-height:100%;" src="'+array[0].Imagenes[0].base64_img+'" />' +
                    '</div>')
                $('#colImages').append('<div class="col-md-2"></div>')
                $('#colImages').append('<div class="col-md-3">' +
                    '<img id="theImg" style="max-width:100%;max-height:100%;" src="'+array[0].Imagenes[1].base64_img+'" />' +
                    '</div>')
                $('#colImages').append('<div class="col-md-2"></div>')
            }else if(array[0].Imagenes.length==3){
                console.log("tres");
                $('#colImages').append('<div class="col-md-3">' +
                    '<img id="theImg" style="max-width:100%;max-height:100%;" src="'+array[0].Imagenes[0].base64_img+'" />' +
                    '</div>')
                $('#colImages').append('<div class="col-md-1"></div>')
                $('#colImages').append('<div class="col-md-3">' +
                    '<img id="theImg" style="max-width:100%;max-height:100%;" src="'+array[0].Imagenes[1].base64_img+'" />' +
                    '</div>')
                $('#colImages').append('<div class="col-md-1"></div>')
                $('#colImages').append('<div class="col-md-3">' +
                    '<img id="theImg" style="max-width:100%;max-height:100%;" src="'+array[0].Imagenes[2].base64_img+'" />' +
                    '</div>')
            }else{
                console.log("cero");
                $('#colImages').append('<h3 class="text-center">Esta descripción no tiene fotos, lo sentimos.</h3>')

            }
               // aClass.href= array[i].Imagenes[f].base64_img;

            /*$('.panel-heading').append('<strong>'+array[0].Tapa[0].Nombre+'</strong><em>('+array[0].Tapa[0].Calle+')</em>');
           for (i = 0; i < array.length; i++) {
               $('.bodyTable').append('<tr id="comen'+ i+'" >');
               currentTD=document.getElementById("comen"+i);
               $('#comen'+i).append("<td>"+array[i].Descripcion+"</td>");
               $('#comen'+i).append("<td>"+array[i].Fecha_publicacion+"</td>");
               $('#comen'+i).append("<td>"+array[i].Precio+" €</td>");
               $('#comen'+i).append("<td>"+array[i].Puntuacion+" puntos</td>");
               $('#comen'+i).append("<td> <button type='submit' onclick='imprimir(this)' class='btn btn-primary center-block'>Ver fotos<p hidden id='idDescripcion'>"+array[i].ID_descripcion+"</p></button> </td>");
               $('.bodyTable').append("</tr>");
           }*/
        }
    });
}

function toDataURL(src, callback, outputFormat) {
    var img = new Image();
    img.crossOrigin = 'Anonymous';
    img.onload = function() {
        var canvas = document.createElement('CANVAS');
        var ctx = canvas.getContext('2d');
        var dataURL;
        canvas.height = this.height;
        canvas.width = this.width;
        ctx.drawImage(this, 0, 0);
        dataURL = canvas.toDataURL(outputFormat);
        callback(dataURL);
    };
    img.src = src;
    if (img.complete || img.complete === undefined) {
        img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
        img.src = src;
    }
}

