<?php
require 'vendor/autoload.php';
require 'class.phpmailer.php';
require 'class.smtp.php';
$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];

$c = new \Slim\Container($configuration);

$app = new \Slim\App($c);

$app->get('/tapas[/[{param_id}]]', function ($request, $response, $args) {

    //Comprueba los parámetros
    if (empty($args['param_id'])){
        getPoints();
        //getDescriptions();
    } else {
        // Comprueba los parámetros
        $param_id = intval($args['param_id']);
        //getDescription($param_id);
        getPoint($param_id);
    }

})->add(function ($request, $response, $next) {

    $tokenUser =  $request->getHeaderLine('HTTP_AUTHORIZATION');
    $sql = 'SELECT * FROM usuario where api_TOKEN = :token AND Activado=1';
    try{
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);
        $stmt->bindParam("token", $tokenUser);
        $stmt->execute();
        $user = $stmt->fetchAll(PDO::FETCH_OBJ);
    }catch (PDOException $e){}
    session_start();

    if(!isset($_SESSION["logueado"])){
        if(sizeof($user)==1){
            $response = $next($request, $response);
            return $response;
        }
        return $this->response->withStatus(401)->withJson(array("error"=>"no autorizado"));
    }
    $response = $next($request, $response);
    return $response;
});

$app->get('/description/[{param_id}]', function ($request, $response, $args) {//Paramtro opcional
    $param_id = intval($args['param_id']);
    getDescription($param_id);
})->add(function ($request, $response, $next) {

    $tokenUser =  $request->getHeaderLine('HTTP_AUTHORIZATION');
    $sql = 'SELECT * FROM usuario where api_TOKEN = :token AND Activado=1';
    try{
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);
        $stmt->bindParam("token", $tokenUser);
        $stmt->execute();
        $user = $stmt->fetchAll(PDO::FETCH_OBJ);
    }catch (PDOException $e){}
    session_start();

    if(!isset($_SESSION["logueado"])){
        if(sizeof($user)==1){
            $response = $next($request, $response);
            return $response;
        }
        return $this->response->withStatus(401)->withJson(array("error"=>"no autorizado"));
    }
    $response = $next($request, $response);
    return $response;
});

$app->get('/descriptionWEB/[{param_id}]', function ($request, $response, $args) {//Paramtro opcional
    $param_id = intval($args['param_id']);
    getDescriptionWEB($param_id);
})->add(function ($request, $response, $next) {

    $tokenUser =  $request->getHeaderLine('HTTP_AUTHORIZATION');
    $sql = 'SELECT * FROM usuario where api_TOKEN = :token AND Activado=1';
    try{
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);
        $stmt->bindParam("token", $tokenUser);
        $stmt->execute();
        $user = $stmt->fetchAll(PDO::FETCH_OBJ);
    }catch (PDOException $e){}
    session_start();

    if(!isset($_SESSION["logueado"])){
        if(sizeof($user)==1){
            $response = $next($request, $response);
            return $response;
        }
        return $this->response->withStatus(401)->withJson(array("error"=>"no autorizado"));
    }
    $response = $next($request, $response);
    return $response;
});

$app->get('/descriptionUser/[{param_id}]', function ($request, $response, $args) {//Paramtro opcional
    $userID = intval($args['param_id']);

    $completeArray =array(); //ARRAY PARA TODO
    $description_array = array();     //ARRAY PARA PRIMERA
    $tapa_array = array();
    $image_array = array();
    $sql='SELECT * FROM descripcion where `Usuario` = :id';
    try{
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $userID);
        $stmt->execute();
        while ($row_description = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
            $description_array['ID_descripcion'] = $row_description[0];
            $idDesctipcion=$row_description[0];
            $user_id = $row_description[1];
            $tapa_id = $row_description[2];
            $description_array['Descripcion'] = $row_description[3];
            $description_array['Precio'] = $row_description[4];
            //$description_array['Imagen'] = $row_description[5];
            $description_array['Fecha_publicacion'] = $row_description[6];
            $description_array['Puntuacion'] = $row_description[7];
            $description_array['Confirmado'] = $row_description[8];

            $description_array['Usuario']=$row_description[1];

            $description_array['Tapa']=array();
            $result3 = $db->query("SELECT * FROM tapa WHERE ID_tapa = '$tapa_id'");
            while($row_tapa = $result3->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                $tapa_array['Nombre']=$row_tapa[2];
                $tapa_array['Categoria']=$row_tapa[1];
                $tapa_array['Calle']=$row_tapa[3];
                $tapa_array['Latitud']=$row_tapa[4];
                $tapa_array['Longitud']=$row_tapa[5];
                $tapa_array['Puntuacion media']=$row_tapa[6];
                array_push($description_array['Tapa'],$tapa_array);
            }

            $description_array['Imagenes']=array();
            $result4 = $db->query("SELECT * FROM `images` WHERE `descripcion_id` = '$idDesctipcion' limit 0");
            while($row_image = $result4->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                $image_array['ID_Imagen']=$row_image[0];
                $image_array['base64_img']=$row_image[2];
                array_push($description_array['Imagenes'],$image_array);
            }
            array_push($completeArray,$description_array);
        }
        $db=null;
        echo json_encode($completeArray,JSON_PRETTY_PRINT);
    }catch(PDOException $ex) {
        echo '{"error":{"text":'. $ex->getMessage() .'}}';
    }
})->add(function ($request, $response, $next) {

    $tokenUser =  $request->getHeaderLine('HTTP_AUTHORIZATION');
    $sql = 'SELECT * FROM usuario where api_TOKEN = :token AND Activado=1';
    try{
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);
        $stmt->bindParam("token", $tokenUser);
        $stmt->execute();
        $user = $stmt->fetchAll(PDO::FETCH_OBJ);
    }catch (PDOException $e){}
    session_start();

    if(!isset($_SESSION["logueado"])){
        if(sizeof($user)==1){
            $response = $next($request, $response);
            return $response;
        }
        return $this->response->withStatus(401)->withJson(array("error"=>"no autorizado"));
    }
    $response = $next($request, $response);
    return $response;
});

$app->get('/getImages/[{param_id}]', function ($request, $response, $args) {//Paramtro opcional
    $descriptID = intval($args['param_id']);
    $completeArray =array(); //ARRAY PARA TODO
    $description_array = array();     //ARRAY PARA PRIMERA

    $sql='SELECT * FROM images where `descripcion_id` = :id';
    try{
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $descriptID);
        $stmt->execute();
        while ($row_description = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
            $description_array['ID_Imagen'] = $row_description[0];
            $description_array['idDesc'] = $row_description[1];
            $description_array['base64_img'] = $row_description[2];
            array_push($completeArray,$description_array);
        }
        $db=null;
        echo json_encode($completeArray,JSON_PRETTY_PRINT);
    }catch(PDOException $ex) {
        echo '{"error":{"text":'. $ex->getMessage() .'}}';
    }
})->add(function ($request, $response, $next) {

    $tokenUser =  $request->getHeaderLine('HTTP_AUTHORIZATION');
    $sql = 'SELECT * FROM usuario where api_TOKEN = :token AND Activado=1';
    try{
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);
        $stmt->bindParam("token", $tokenUser);
        $stmt->execute();
        $user = $stmt->fetchAll(PDO::FETCH_OBJ);
    }catch (PDOException $e){}
    session_start();

    if(!isset($_SESSION["logueado"])){
        if(sizeof($user)==1){
            $response = $next($request, $response);
            return $response;
        }
        return $this->response->withStatus(401)->withJson(array("error"=>"no autorizado"));
    }
    $response = $next($request, $response);
    return $response;
});

$app->post('/changePassword', function ($request, $response, $args) {//Paramtro opcional
    $data = $request->getParsedBody();
    $correo = $data['correo'];
    $correo = strtolower($correo);//Comprobar que exite
    $pass=$data['password'];//El que recibo

    if(isset($correo,$pass)){
        $sql = 'UPDATE `usuario` SET `Password` = :pass WHERE `usuario`.`Correo` = :mail;';
        if((checkMail($correo)==true)){
            try {
                $dbCon = getConnection();
                $stmt = $dbCon->prepare($sql);
                $passmd5=md5($pass);
                $stmt->bindParam("mail", $correo);
                $stmt->bindParam("pass", $passmd5);
                $stmt->execute();
                $dbCon = null;
            } catch(PDOException $e) {
            }
        }else{
            return $this->response->withStatus(501)->withJson(array("error"=>"Ha habido un problema cambiando la contraseña"));
        }
    }else{
        return $this->response->withStatus(500)->withJson(array("error"=>"json error"));
    }
    return $this->response->withStatus(200)->withJson(array("status"=>"ok"));



})->add(function ($request, $response, $next) {
    $tokenUser =  $request->getHeaderLine('HTTP_AUTHORIZATION');
    $sql = 'SELECT * FROM usuario where api_TOKEN = :token AND Activado=1';
    try{
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);
        $stmt->bindParam("token", $tokenUser);
        $stmt->execute();
        $user = $stmt->fetchAll(PDO::FETCH_OBJ);
    }catch (PDOException $e){}
    session_start();

    if(!isset($_SESSION["logueado"])){
        if(sizeof($user)==1){
            $response = $next($request, $response);
            return $response;
        }
        return $this->response->withStatus(401)->withJson(array("error"=>"no autorizado"));
    }
    $response = $next($request, $response);
    return $response;
});

$app->get('/categorias', function ($request, $response, $args) {
    getCategories();
})->add(function ($request, $response, $next) {

    $tokenUser =  $request->getHeaderLine('HTTP_AUTHORIZATION');
    $sql = 'SELECT * FROM usuario where api_TOKEN = :token AND Activado=1';
    try{
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);
        $stmt->bindParam("token", $tokenUser);
        $stmt->execute();
        $user = $stmt->fetchAll(PDO::FETCH_OBJ);
    }catch (PDOException $e){}
    session_start();

    if(!isset($_SESSION["logueado"])){
        if(sizeof($user)==1){
            $response = $next($request, $response);
            return $response;
        }
        return $this->response->withStatus(401)->withJson(array("error"=>"no autorizado"));
    }
    $response = $next($request, $response);
    return $response;
});

$app->get('/statusLogin', function ($request, $response, $args) {
    session_start();
    return $this->response->withJson(array("ok"=>$_SESSION['nombre']));
});

$app->post('/lostPassword', function ($request, $response, $args) {
    $data = $request->getParsedBody();
    $correo = $data['correo'];
    $nick = $data['nick'];
    $correo = strtolower($correo);

    if(isset($correo,$nick)){
        if(checkFullUser($correo,$nick)==true){//si existe
            $nuevaPassCorreo=generaPass();
            $nuevaPassBD=md5($nuevaPassCorreo);
            $sql = "UPDATE `usuario` SET `Password` = :pass WHERE Nick like :nick and Correo like :mail";

            try {
                $dbCon = getConnection();
                $stmt = $dbCon->prepare($sql);
                $stmt->bindParam("pass", $nuevaPassBD);
                $stmt->bindParam("nick", $nick);
                $stmt->bindParam("mail", $correo);
                $stmt->execute();
                sendPassword($correo,$nick,$nuevaPassCorreo);
                $dbCon = null;
            } catch(PDOException $e) {
            }
            return $this->response->withStatus(200)->withJson(array("status"=>"ok"));


        }else{
            return $this->response->withStatus(200)->withJson(array("status"=>"ok"));

        }


    }else{
        return $this->response->withStatus(500)->withJson(array("error"=>"json error"));
    }

    return $this->response->withStatus(200)->withJson(array("status"=>"ok"));



});

$app->post('/login', function ($request, $response, $args) {
    $data = $request->getParsedBody();
    $correoJSon=$data['mail'];
    $password=$data['pass'];
    $correo = strtolower($correoJSon);
    $loginPassword = md5($password);


    $sql = 'SELECT * FROM usuario where Correo = :correo and Password = :pass AND Activado=1';
    try {
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);
        $stmt->bindParam("correo", $correo);
        $stmt->bindParam("pass", $loginPassword);
        $stmt->execute();
        while ($row_description = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
            $userok = $row_description[1];
            $userId = $row_description[0];
            $passok = $row_description[5];
            $usermail= $row_description[4];
            $userTOKEN = $row_description[9];
            $TOKEN_Expiration = $row_description[10];
        }
        $dbCon = null;

    } catch(PDOException $e) {}
    if(isset($correo) && isset($loginPassword)) {

        if($correo == $usermail && $loginPassword == $passok) {
            session_start();
            $_SESSION["logueado"] = TRUE;
            $_SESSION['nombre']= $userok;
            $_SESSION['userid']   =$userId;

            $hoy = date("Y-m-d");
            if(($userTOKEN=="") || ($hoy>$TOKEN_Expiration)){
                $dbCon = getConnection();
                $userTOKEN = obtenToken(20);
                $nuevafecha = strtotime ( '+20 day' , strtotime ( $hoy ) ) ;
                $fechaExpiracion = date ( 'Y-m-j' , $nuevafecha );
                $insertToken="UPDATE `usuario` SET `api_TOKEN` = '$userTOKEN', `TOKEN_expiracion` = '$fechaExpiracion' WHERE `usuario`.`ID_user` = '$userId'";
                $stmt = $dbCon->query($insertToken);
                $stmt->execute();
                $dbCon = null;
            }
            return $this->response->withJson(array("ok"=>$userTOKEN));
        }else {
            return $this->response->withStatus(401)->withJson(array("error"=>"222222"));
        }

    }else{
        return $this->response->withStatus(401)->withJson(array("error"=>"wrong data"));
    }


});

$app->get('/logout', function ($request, $response, $args) {
    session_start();
    session_destroy();
    return $this->response->withJson(array("ok"=>"sesion finalizada"));
});

$app->post('/tapa', function ($request, $response, $args) {
    $data = $request->getParsedBody();
    $nombre=$data['name'];
    $latitud=$data['lat'];
    $longitud=$data['long'];
    $direccion=$data['street'];
    $categoria='Tapa';
    $precio=$data['price'];
    $puntuacion=$data['punctuation'];
    $img1=$data['img1'];
    $img2=$data['img2'];
    $img3=$data['img3'];
    $user=$data['idUser'];
    $descripcion=$data['descripcion'];

    if(isset($nombre,$latitud,$longitud,$direccion,$categoria,$precio,$descripcion,$user,$puntuacion)) {
        $sql = 'INSERT INTO `tapa` (`Categoria`, `Nombre`, `Calle`, `Latitud`, `Longitud`, `Puntuacion`, `activada`) VALUES (:cat , :nombre, :calle ,:latitud , :longitud ,0 , \'-1\')';
        try {

            $dbCon = getConnection();
            $stmt = $dbCon->prepare($sql);
            $stmt->bindParam("cat", $categoria);
            $stmt->bindParam("nombre", $nombre);
            $stmt->bindParam("calle", $direccion);
            $stmt->bindParam("latitud", $latitud);
            $stmt->bindParam("longitud", $longitud);
            $stmt->execute();

            $ultimoId = $dbCon->lastInsertId();

            $dbCon = null;

            $sql = 'INSERT INTO `descripcion` (`Usuario`, `Tapa`, `Descripcion`, `Precio`, `Imagen`, `Puntuacion`, `Confirmado`) VALUES (:user, :idTapa, :descripcion, :precio, NULL, :puntuacion, \'-1\')';
            try {
                $dbCon = getConnection();
                $stmt = $dbCon->prepare($sql);
                $stmt->bindParam("user", $user);
                $stmt->bindParam("idTapa", $ultimoId);
                $stmt->bindParam("descripcion", $descripcion);
                $stmt->bindParam("puntuacion", $puntuacion);
                $stmt->bindParam("precio", $precio);
                $stmt->execute();
                $ultimoId = $dbCon->lastInsertId();
                if ($img1 != "") {
                    $sql = 'INSERT INTO `images` ( `descripcion_id`, `base64_img`) VALUES ( :lastId, :img)';
                    try {
                        $dbCon = getConnection();
                        $stmt = $dbCon->prepare($sql);
                        $stmt->bindParam("lastId", $ultimoId);
                        $stmt->bindParam("img", $img1);
                        $stmt->execute();
                        $dbCon = null;
                    } catch (PDOException $e) {
                        return $this->response->withStatus(500)->withJson(array("status"=>e));
                    }
                }
                if ($img2 != "") {
                    $sql = 'INSERT INTO `images` ( `descripcion_id`, `base64_img`) VALUES ( :lastId, :img)';
                    try {
                        $dbCon = getConnection();
                        $stmt = $dbCon->prepare($sql);
                        $stmt->bindParam("lastId", $ultimoId);
                        $stmt->bindParam("img", $img2);
                        $stmt->execute();
                        $dbCon = null;
                    } catch (PDOException $e) {
                        return $this->response->withStatus(500)->withJson(array("status"=>e));
                    }
                }
                if ($img3 != "") {
                    $sql = 'INSERT INTO `images` ( `descripcion_id`, `base64_img`) VALUES ( :lastId, :img)';
                    try {
                        $dbCon = getConnection();
                        $stmt = $dbCon->prepare($sql);
                        $stmt->bindParam("lastId", $ultimoId);
                        $stmt->bindParam("img", $img3);
                        $stmt->execute();
                        $dbCon = null;
                    } catch (PDOException $e) {
                        return $this->response->withStatus(500)->withJson(array("status"=>e));
                    }
                }
                $dbCon = null;

            } catch (PDOException $e) {
                return $this->response->withStatus(500)->withJson(array("status"=>e));
            }
            return $this->response->withStatus(200)->withJson(array("status"=>"ok"));
        }catch (PDOException $e) {
            return $this->response->withStatus(500)->withJson(array("status"=>e));
        }
    }else{
        return $this->response->withStatus(500)->withJson(array("status"=>"json malformed"));
    }
})->add(function ($request, $response, $next) {

    $tokenUser =  $request->getHeaderLine('HTTP_AUTHORIZATION');
    $sql = 'SELECT * FROM usuario where api_TOKEN = :token AND Activado=1';
    try{
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);
        $stmt->bindParam("token", $tokenUser);
        $stmt->execute();
        $user = $stmt->fetchAll(PDO::FETCH_OBJ);
    }catch (PDOException $e){}
    session_start();

    if(!isset($_SESSION["logueado"])){
        if(sizeof($user)==1){
            $response = $next($request, $response);
            return $response;
        }
        return $this->response->withStatus(401)->withJson(array("error"=>"no autorizado"));
    }
    $response = $next($request, $response);
    return $response;
});

$app->post('/newComment', function ($request, $response, $args) {
    $data = $request->getParsedBody();
    $desc=$data['descripcion'];
    $idTapa=$data['idTapa'];
    $idUser=$data['idUser'];
    $img1=$data['img1'];
    $img2=$data['img2'];
    $img3=$data['img3'];
    $precio=$data['price'];
    $points=$data['punctuation'];


    if(isset($desc,$idTapa,$idUser,$img1,$img2,$img3,$precio,$points)){
        $sql = 'INSERT INTO `descripcion` (`Usuario`, `Tapa`, `Descripcion`, `Precio`, `Puntuacion`, `Confirmado`) VALUES ( :user, :tapaId, :desc, :precio, :punt, \'-1\')';
        try {
            $dbCon = getConnection();
            $stmt = $dbCon->prepare($sql);
            $stmt->bindParam("user", $idUser);
            $stmt->bindParam("tapaId", $idTapa);
            $stmt->bindParam("desc", $desc);
            $stmt->bindParam("precio", $precio);
            $stmt->bindParam("punt", $points);
            $stmt->execute();
            $ultimoId=$dbCon->lastInsertId();
            if($img1!=""){
                $sql='INSERT INTO `images` ( `descripcion_id`, `base64_img`) VALUES ( :lastId, :img)';
                try {
                    $dbCon = getConnection();
                    $stmt = $dbCon->prepare($sql);
                    $stmt->bindParam("lastId", $ultimoId);
                    $stmt->bindParam("img", $img1);
                    $stmt->execute();
                    $dbCon = null;
                } catch(PDOException $e) {
                    return $this->response->withStatus(500)->withJson(array("status"=>$e));

                }
            }
            if($img2!=""){
                $blobImage=base64_decode($img2);
                $sql='INSERT INTO `images` ( `descripcion_id`, `base64_img`) VALUES ( :lastId, :img)';
                try {
                    $dbCon = getConnection();
                    $stmt = $dbCon->prepare($sql);
                    $stmt->bindParam("lastId", $ultimoId);
                    $stmt->bindParam("img", $blobImage);
                    $stmt->execute();
                    $dbCon = null;
                } catch(PDOException $e) {
                    return $this->response->withStatus(500)->withJson(array("status"=>e));

                }
            }
            if($img3!=""){
                $blobImage=base64_decode($img3);
                $sql='INSERT INTO `images` ( `descripcion_id`, `base64_img`) VALUES ( :lastId, :img)';
                try {
                    $dbCon = getConnection();
                    $stmt = $dbCon->prepare($sql);
                    $stmt->bindParam("lastId", $ultimoId);
                    $stmt->bindParam("img", $blobImage);
                    $stmt->execute();
                    $dbCon = null;
                } catch(PDOException $e) {
                    return $this->response->withStatus(500)->withJson(array("status"=>e));
                }
            }
            $dbCon = null;
            return $this->response->withStatus(200)->withJson(array("status"=>"ok"));
        } catch(PDOException $e) {
        }
    }else{
        return $this->response->withStatus(500)->withJson(array("status"=>"json malformed"));
    }


})->add(function ($request, $response, $next) {

    $tokenUser =  $request->getHeaderLine('HTTP_AUTHORIZATION');
    $sql = 'SELECT * FROM usuario where api_TOKEN = :token AND Activado=1';
    try{
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);
        $stmt->bindParam("token", $tokenUser);
        $stmt->execute();
        $user = $stmt->fetchAll(PDO::FETCH_OBJ);
    }catch (PDOException $e){}
    session_start();

    if(!isset($_SESSION["logueado"])){
        if(sizeof($user)==1){
            $response = $next($request, $response);
            return $response;
        }
        return $this->response->withStatus(401)->withJson(array("error"=>"no autorizado"));
    }
    $response = $next($request, $response);
    return $response;
});

$app->post('/newUser', function ($request, $response, $args) {
    $data = $request->getParsedBody();
    $name=$data['name'];
    $lastName=$data['lastName'];
    $pass=$data['password'];
    $mail=$data['mail'];
    $user=$data['user'];
    $user = strtolower($user);
    $mail = strtolower($mail);


    if(isset($name,$lastName,$pass,$mail,$user)){
        $sql = 'INSERT INTO `usuario` ( `Nombre`, `Apellido`, `Nick`, `Correo`, `Password`, `Activado`, `TipoUsuario`) VALUES (:name, :secondName, :nick, :mail, :pass, \'1\', \'User\')';
        if((checkMail($mail)==false)&&(checkNick($user)==false)){
            try {
                $dbCon = getConnection();
                $stmt = $dbCon->prepare($sql);
                $passmd5=md5($pass);
                $stmt->bindParam("name", $name);
                $stmt->bindParam("secondName", $lastName);
                $stmt->bindParam("nick", $user);
                $stmt->bindParam("mail", $mail);
                $stmt->bindParam("pass", $passmd5);
                $stmt->execute();
                //sendRegisterMail($user,$mail);
                $dbCon = null;
            } catch(PDOException $e) {
            }
        }else{
            return $this->response->withStatus(501)->withJson(array("error"=>"Ya existe alguien con ese nick o correo"));
        }
    }else{
        return $this->response->withStatus(500)->withJson(array("error"=>"json error"));
    }
    return $this->response->withStatus(200)->withJson(array("status"=>"ok"));

});

$app->get('/userData', function ($request, $response, $args) {
    $token=$request->getHeaderLine('HTTP_AUTHORIZATION');
    getUserData($token);
})->add(function ($request, $response, $next) {

    $tokenUser =  $request->getHeaderLine('HTTP_AUTHORIZATION');
    $sql = 'SELECT * FROM usuario where api_TOKEN = :token AND Activado=1';
    try{
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);
        $stmt->bindParam("token", $tokenUser);
        $stmt->execute();
        $user = $stmt->fetchAll(PDO::FETCH_OBJ);
    }catch (PDOException $e){}
    session_start();

    if(!isset($_SESSION["logueado"])){
        if(sizeof($user)==1){
            $response = $next($request, $response);
            return $response;
        }
        return $this->response->withStatus(401)->withJson(array("error"=>"no autorizado"));
    }
    $response = $next($request, $response);
    return $response;
});

$app->run();

function sendPassword($mailUser, $nick, $pass){
    $mail = new PHPMailer();
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->Debugoutput = 'html';
    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 587;
    $mail->SMTPSecure = 'tls';
    $mail->SMTPAuth = true;
    $mail->Username = "konradkowal213@gmail.com";
    $mail->Password = "konrad213";
    $mail->addAddress($mailUser, $nick);//indicar destinatario y nombre
    $mail->Subject = 'Mas Tapas APP';
    $mail->msgHTML('Tu nueva contraseña es '.$pass);
    $mail->send();
}

function sendRegisterMail($user,$mailUser){
    $mail = new PHPMailer();
    $mail->isSMTP();
    //poner 0 en produccion
    $mail->SMTPDebug = 0;
    $mail->Debugoutput = 'html';
    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 587;
    $mail->SMTPSecure = 'tls';
    $mail->SMTPAuth = true;
    $mail->Username = "konradkowal213@gmail.com";
    $mail->Password = "konrad213";
    $mail->addAddress($mailUser, $user);//indicar destinatario y nombre
    $mail->Subject = 'Mas Tapas APP';
    $mail->msgHTML('Gracias por registarte '.$user);
    $mail->send();
}

function generaPass(){
    //Se define una cadena de caractares. Te recomiendo que uses esta.
    $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    //Obtenemos la longitud de la cadena de caracteres
    $longitudCadena=strlen($cadena);

    //Se define la variable que va a contener la contraseña
    $pass = "";
    //Se define la longitud de la contraseña, en mi caso 10, pero puedes poner la longitud que quieras
    $longitudPass=7;

    //Creamos la contraseña
    for($i=1 ; $i<=$longitudPass ; $i++){
        //Definimos numero aleatorio entre 0 y la longitud de la cadena de caracteres-1
        $pos=rand(0,$longitudCadena-1);

        //Vamos formando la contraseña en cada iteraccion del bucle, añadiendo a la cadena $pass la letra correspondiente a la posicion $pos en la cadena de caracteres definida.
        $pass .= substr($cadena,$pos,1);
    }
    return $pass;
}

function insertimg($img,$ultimoId){
    $sql='INSERT INTO `images` ( `descripcion_id`, `base64_img`) VALUES ( :lastId, :img)';
    try {
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);
        $stmt->bindParam("lastId", $ultimoId);
        $stmt->bindParam("img", $img);
        $stmt->execute();
        $dbCon = null;
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function saveCommentt($desc,$idTapa,$idUser,$img1,$img2,$img3,$precio,$points){
    $sql = 'INSERT INTO `descripcion` (`Usuario`, `Tapa`, `Descripcion`, `Precio`, `Puntuacion`, `Confirmado`) VALUES ( :user, :tapaId, :desc, :precio, :punt, \'0\')';
    try {
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);
        $stmt->bindParam("user", $idUser);
        $stmt->bindParam("tapaId", $idTapa);
        $stmt->bindParam("desc", $desc);
        $stmt->bindParam("precio", $precio);
        $stmt->bindParam("punt", $points);
        $stmt->execute();
        $ultimoId=$dbCon->lastInsertId();
        if($img1!=""){
            insertimg($img1,$ultimoId);
        }
        if($img2!=""){
            insertimg($img2,$ultimoId);
        }
        if($img3!=""){
            insertimg($img3,$ultimoId);
        }
        $dbCon = null;
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function checkMail($mail){
    $sql = "SELECT * FROM `usuario` WHERE `Correo` like '$mail'";
    try {
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);
        $stmt->bindParam("mail", $mail);
        $stmt->execute();
        $correos=$stmt->fetchAll(PDO::FETCH_OBJ);
        $dbCon = null;
    } catch(PDOException $e) {

    }

    if(isset($correos[0])){
        return true;
    }else{
        return false;
    }
}

function checkFullUser($mail, $nick){
    $sql = "SELECT * FROM `usuario` WHERE `Correo` like '$mail' and `Nick` like '$nick'";
    try {
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);
        $stmt->bindParam("mail", $mail);
        $stmt->bindParam("nick", $nick);

        $stmt->execute();
        $correos=$stmt->fetchAll(PDO::FETCH_OBJ);
        $dbCon = null;
    } catch(PDOException $e) {

    }

    if(isset($correos[0])){
        return true;
    }else{
        return false;
    }
}

function checkNick($nick){
    $sql = "SELECT * FROM `usuario` WHERE `Nick` like '$nick'";
    try {
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);
        $stmt->bindParam("nick", $nick);
        $stmt->execute();
        $correos=$stmt->fetchAll(PDO::FETCH_OBJ);
        $dbCon = null;
    } catch(PDOException $e) {
    }
    if(isset($correos[0])){
        return true;
    }else{
        return false;
    }
}

function saveLocalAndDesc($nombre,$latitud,$longitud,$direccion,$categoria,$precio,$descripcion,$user,$puntuacion,$img1,$img2,$img3){
    $sql = 'INSERT INTO `tapa` (`Categoria`, `Nombre`, `Calle`, `Latitud`, `Longitud`, `Puntuacion`, `activada`) VALUES (:cat , :nombre, :calle ,:latitud , :longitud ,0 , false)';
    try {

        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);
        $nombreCorrecto = explode(",", $nombre);
        $stmt->bindParam("cat", $categoria);
        $stmt->bindParam("nombre", $nombreCorrecto[0]);
        $stmt->bindParam("calle", $direccion);
        $stmt->bindParam("latitud", $latitud);
        $stmt->bindParam("longitud", $longitud);
        $stmt->execute();

        $ultimoId=$dbCon->lastInsertId();

        $dbCon = null;

        saveComment($precio,$descripcion,$ultimoId,$user,$puntuacion,$img1,$img2,$img3);

    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function saveComment($precio,$descripcion,$ultimoId,$user,$puntuacion,$img1,$img2,$img3){
    $sql = 'INSERT INTO `descripcion` (`Usuario`, `Tapa`, `Descripcion`, `Precio`, `Imagen`, `Puntuacion`, `Confirmado`) VALUES (:user, :idTapa, :descripcion, :precio, NULL, :puntuacion, \'0\')';
    try {
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);
        $stmt->bindParam("user", $user);
        $stmt->bindParam("idTapa", $ultimoId);
        $stmt->bindParam("descripcion", $descripcion);
        $stmt->bindParam("puntuacion", $puntuacion);
        $stmt->bindParam("precio", $precio);
        $stmt->execute();
        $ultimoId=$dbCon->lastInsertId();
        if($img1!=""){
            insertimg($img1,$ultimoId);
        }
        if($img2!=""){
            insertimg($img2,$ultimoId);
        }
        if($img3!=""){
            insertimg($img3,$ultimoId);
        }
        $dbCon = null;

    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function getPoints(){

    $sql = 'SELECT *,tapa.puntuacion as "Puntuacion media" FROM tapa, descripcion WHERE tapa.ID_tapa=descripcion.Tapa and activada=true GROUP by tapa.ID_tapa';
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $wines = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($wines,JSON_PRETTY_PRINT);
    } catch(PDOException $ex) {
        echo '{"error":{"text":'. $ex->getMessage() .'}}';
    }
}

function getCategories(){
    $sql = 'SELECT * FROM `categoria`';
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $wines = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        echo json_encode($wines,JSON_PRETTY_PRINT);
    } catch(PDOException $ex) {
        echo '{"error":{"text":'. $ex->getMessage() .'}}';
    }
}

function getDescriptions(){
    $completeArray =array(); //ARRAY PARA TODO
    $description_array = array();     //ARRAY PARA PRIMERA
    $user_array = array();     //ARRAY PARA SUBCONSULTA
    $tapa_array = array();
    $image_array = array();
    $sql='SELECT * FROM descripcion';
    try{
        $db = getConnection();
        $result = $db->query($sql);

        while ($row_description = $result->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
            $description_array['ID_descripcion'] = $row_description[0];
            $idDesctipcion=$row_description[0];
            $user_id = $row_description[1];
            $tapa_id = $row_description[2];
            $description_array['Descripcion'] = $row_description[3];
            $description_array['Precio'] = $row_description[4];
            //$description_array['Imagen'] = $row_description[5];
            $description_array['Fecha_publicacion'] = $row_description[6];
            $description_array['Puntuacion'] = $row_description[7];
            $description_array['Confirmado'] = $row_description[8];

            $description_array['Usuario']=array();
            $result2 = $db->query("SELECT * FROM usuario WHERE ID_user = '$user_id'");
            while($row_user = $result2->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                //$user_array['ID_user']=$row_user['ID_user'];
                //$user_array['Nombre']=$row_user['Nombre'];
                //$user_array['Apellido']=$row_user['Apellido'];
                $user_array['Nick']=$row_user[3];
                //$user_array['Correo']=$row_user['Correo'];

                array_push($description_array['Usuario'],$user_array);
            }

            $description_array['Tapa']=array();
            $result3 = $db->query("SELECT * FROM tapa WHERE ID_tapa = '$tapa_id'");
            while($row_tapa = $result3->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                $tapa_array['Nombre']=$row_tapa[2];
                $tapa_array['Categoria']=$row_tapa[1];
                $tapa_array['Calle']=$row_tapa[3];
                $tapa_array['Latitud']=$row_tapa[4];
                $tapa_array['Longitud']=$row_tapa[5];
                $tapa_array['Puntuacion media']=$row_tapa[6];
                array_push($description_array['Tapa'],$tapa_array);
            }

            $description_array['Imagenes']=array();
            $result4 = $db->query("SELECT * FROM `images` WHERE `descripcion_id` = '$idDesctipcion'");
            while($row_image = $result4->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                $image_array['ID_Imagen']=$row_image[0];
                $image_array['base64_img']=$row_image[2];
                array_push($description_array['Imagenes'],$image_array);
            }
            array_push($completeArray,$description_array);
        }
        $db=null;
        echo json_encode($completeArray,JSON_PRETTY_PRINT);
    }catch(PDOException $ex) {
        echo '{"error":{"text":'. $ex->getMessage() .'}}';
    }

}

function getPoint($id){
    $sql = "SELECT * from tapa INNER join descripcion on tapa.ID_tapa= descripcion.Tapa WHERE ID_tapa =:id and confirmado = 1";
    try {
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $user = $stmt->fetchAll(PDO::FETCH_OBJ);
        $dbCon = null;
        echo json_encode($user, JSON_PRETTY_PRINT);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function getDescription($id){
    $completeArray =array(); //ARRAY PARA TODO
    $description_array = array();     //ARRAY PARA PRIMERA
    $user_array = array();     //ARRAY PARA SUBCONSULTA
    $tapa_array = array();
    $image_array = array();
    $sql='SELECT * FROM descripcion where `Tapa` = :id and confirmado = 1';
    try{
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        while ($row_description = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
            $description_array['ID_descripcion'] = $row_description[0];
            $idDesctipcion=$row_description[0];
            $user_id = $row_description[1];
            $tapa_id = $row_description[2];
            $description_array['Descripcion'] = $row_description[3];
            $description_array['Precio'] = $row_description[4];
            //$description_array['Imagen'] = $row_description[5];
            $description_array['Fecha_publicacion'] = $row_description[6];
            $description_array['Puntuacion'] = $row_description[7];
            $description_array['Confirmado'] = $row_description[8];

            $description_array['Usuario']=array();
            $result2 = $db->query("SELECT * FROM usuario WHERE ID_user = '$user_id'");
            while($row_user = $result2->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                //$user_array['ID_user']=$row_user['ID_user'];
                //$user_array['Nombre']=$row_user['Nombre'];
                //$user_array['Apellido']=$row_user['Apellido'];
                $user_array['Nick']=$row_user[3];
                //$user_array['Correo']=$row_user['Correo'];

                array_push($description_array['Usuario'],$user_array);
            }

            $description_array['Tapa']=array();
            $result3 = $db->query("SELECT * FROM tapa WHERE ID_tapa = '$tapa_id'");
            while($row_tapa = $result3->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                $tapa_array['Nombre']=$row_tapa[2];
                $tapa_array['Categoria']=$row_tapa[1];
                $tapa_array['Calle']=$row_tapa[3];
                $tapa_array['Latitud']=$row_tapa[4];
                $tapa_array['Longitud']=$row_tapa[5];
                $tapa_array['Puntuacion media']=$row_tapa[6];
                $tapa_array['IDTapa']=$row_tapa[0];
                array_push($description_array['Tapa'],$tapa_array);
            }

            $description_array['Imagenes']=array();
            $result4 = $db->query("SELECT * FROM `images` WHERE `descripcion_id` = '$idDesctipcion' limit 0");
            while($row_image = $result4->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                $image_array['ID_Imagen']=$row_image[0];
                $image_array['base64_img']=$row_image[2];
                array_push($description_array['Imagenes'],$image_array);
            }
            array_push($completeArray,$description_array);
        }
        $db=null;
        echo json_encode($completeArray,JSON_PRETTY_PRINT);
    }catch(PDOException $ex) {
        echo '{"error":{"text":'. $ex->getMessage() .'}}';
    }

}

function getDescriptionWEB($id){
    $completeArray =array(); //ARRAY PARA TODO
    $description_array = array();     //ARRAY PARA PRIMERA
    $user_array = array();     //ARRAY PARA SUBCONSULTA
    $tapa_array = array();
    $image_array = array();
    $sql='SELECT * FROM descripcion where `ID_descripcion` = :id /*and confirmado = 1*/';
    try{
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        while ($row_description = $stmt->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
            $description_array['ID_descripcion'] = $row_description[0];
            $idDesctipcion=$row_description[0];
            $user_id = $row_description[1];
            $tapa_id = $row_description[2];
            $description_array['Descripcion'] = $row_description[3];
            $description_array['Precio'] = $row_description[4];
            //$description_array['Imagen'] = $row_description[5];
            $description_array['Fecha_publicacion'] = $row_description[6];
            $description_array['Puntuacion'] = $row_description[7];
            $description_array['Confirmado'] = $row_description[8];

            $description_array['Usuario']=array();
            $result2 = $db->query("SELECT * FROM usuario WHERE ID_user = '$user_id'");
            while($row_user = $result2->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                //$user_array['ID_user']=$row_user['ID_user'];
                //$user_array['Nombre']=$row_user['Nombre'];
                //$user_array['Apellido']=$row_user['Apellido'];
                $user_array['Nick']=$row_user[3];
                //$user_array['Correo']=$row_user['Correo'];

                array_push($description_array['Usuario'],$user_array);
            }

            $description_array['Tapa']=array();
            $result3 = $db->query("SELECT * FROM tapa WHERE ID_tapa = '$tapa_id'");
            while($row_tapa = $result3->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                $tapa_array['Nombre']=$row_tapa[2];
                $tapa_array['Categoria']=$row_tapa[1];
                $tapa_array['Calle']=$row_tapa[3];
                $tapa_array['Latitud']=$row_tapa[4];
                $tapa_array['Longitud']=$row_tapa[5];
                $tapa_array['Puntuacion media']=$row_tapa[6];
                array_push($description_array['Tapa'],$tapa_array);
            }

            $description_array['Imagenes']=array();
            $result4 = $db->query("SELECT * FROM `images` WHERE `descripcion_id` = '$idDesctipcion' limit 3");
            while($row_image = $result4->fetch(PDO::FETCH_NUM, PDO::FETCH_ORI_NEXT)){
                $image_array['ID_Imagen']=$row_image[0];
                $blobFile=$row_image[2];
                $startName='data:image/jpeg;base64,';
                $fullFile=$startName.$blobFile;
                $image_array['base64_img']=($blobFile);
                array_push($description_array['Imagenes'],$image_array);
            }
            array_push($completeArray,$description_array);
        }
        $db=null;
        echo json_encode($completeArray,JSON_PRETTY_PRINT);
    }catch(PDOException $ex) {
        echo '{"error":{"text":'. $ex->getMessage() .'}}';
    }

}

function getUserData($token){
    $sql = "SELECT ID_user, Nick, TOKEN_expiracion from usuario where api_TOKEN =:token and Activado = 1";
    try {
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);
        $stmt->bindParam("token", $token);
        $stmt->execute();
        $user = $stmt->fetchAll(PDO::FETCH_OBJ);
        $dbCon = null;
        echo json_encode($user, JSON_PRETTY_PRINT);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function getConnection() {
    $dbhost='localhost';
    //$dbuser='mastapas';
      $dbuser='root';
    // $dbpass='Igx43h$4';
    $dbpass='';
    // $dbname='mastapas_database';
    $dbname='mastapas';
    $db = new PDO("mysql:host=$dbhost;dbname=$dbname;charset=utf8mb4", $dbuser, $dbpass);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $db;
}

function obtenCaracterAleatorio($arreglo) {
    $clave_aleatoria = array_rand($arreglo, 1);	//obtén clave aleatoria
    return $arreglo[ $clave_aleatoria ];	//devolver ítem aleatorio
}

function obtenCaracterMd5($car) {
    $md5Car = md5($car.Time());	//Codificar el carácter y el tiempo POSIX (timestamp) en md5
    $arrCar = str_split(strtoupper($md5Car));	//Convertir a array el md5
    $carToken = obtenCaracterAleatorio($arrCar);	//obtén un ítem aleatoriamente
    return $carToken;
}

function obtenToken($longitud) {
    //crear alfabeto
    $mayus = "ABCDEFGHIJKMNPQRSTUVWXYZ";
    $mayusculas = str_split($mayus);	//Convertir a array
    //crear array de numeros 0-9
    $numeros = range(0,9);
    //revolver arrays
    shuffle($mayusculas);
    shuffle($numeros);
    //Unir arrays
    $arregloTotal = array_merge($mayusculas,$numeros);
    $newToken = "";
    for($i=0;$i<=$longitud;$i++) {
        $miCar = obtenCaracterAleatorio($arregloTotal);
        $newToken .= obtenCaracterMd5($miCar);
    }
    return $newToken;
}

?>