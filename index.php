<?php
error_reporting(0);

session_start();
if($_SESSION["logueado"] == TRUE) {
    header("Location: map.php");
}
if(isset($_GET["error"]) && $_GET["error"] != "login") {
    header("Location: index.php");
  }

 ?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mas tapas</title>
    <link rel="icon" href="img/ic_launcher.png">

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
<script src="js/BD.js"></script>
    <script src="js/jquery-3.2.1.min.js"></script>
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body background="img/hexagonal-pattern_1051-833.jpg">
	
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default"><img style="max-width: 100%; max-height: 100%" src="img/logo.png">
				<div class="panel-body">
					<form role="form" class="login-form" action="" method="post">
						<fieldset>
                            <?php
                            if(isset($_GET["error"])) {
                                echo '<p style="color:red;">Usuario y / o Contrasea erroneos. Intentelo de nuevo.</p>';
                            }
                            ?>
							<div class="form-group">
                                <input class="form-control" placeholder="Mail..." name="mail" type="text" >
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Password" name="pass" type="password" value="">
							</div>

                        </fieldset>
					</form>
                    <button onclick="login()" class="btn btn-primary">Login</button>
                    <a href="registro.html" class="btn ">Registrate!</a>
                    <script>
                        $(document).keypress(function(e) {
                            if(e.which == 13) {
                                login();
                            }
                        });
                    </script>
                    <a href="newPassword.html" class="btn ">Has olvidado la contraseña?</a>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->	
	
		

	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
    <script src="js/AjaxFunctions.js"></script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){		  
				$(this).find('em:first').toggleClass("glyphicon-minus");	  
			}); 
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
</body>

</html>
