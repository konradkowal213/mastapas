<?php

session_start();
if($_SESSION["logueado"] == TRUE) {

    header('X-Frame-Options: GOFORIT');
    ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mas tapas</title>
    <link rel="icon" href="img/ic_launcher.png">    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/jsonTransform.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/datepicker3.css" rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
<link href="css/modalStyle.css" rel="stylesheet">
<!--Icons-->
<script src="js/lumino.glyphs.js"></script>
<script src="js/addPoint.js"></script>
<script src="js/AjaxFunctions.js"></script>




    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDeItUwGnJ5wL0jIpKqu9xPV3hMIsAcDNo&libraries=places" async defer></script>

    <!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body >

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" style="font-family: miFuente" href="map.php"><span>Mas</span>Tapas</a>
        </div>
    </div><!-- /.container-fluid -->
</nav>

<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <ul class="nav menu">
        <li><a href="map.php"><svg class="glyph stroked location pin"><use xlink:href="#stroked-location-pin"/></svg>Map</a></li>
        <li><a href="newPoint.php"><svg class="glyph stroked home"><use xlink:href="#stroked-home"/></svg>Nuevo establecimiento</a></li>
        <li><a href="misPublicaciones.php"><svg class="glyph stroked line-graph"><use xlink:href="#stroked-line-graph"></use></svg>Mis publicaciones</a></li>
        <li><a href="changePass.php"><svg class="glyph stroked lock"><use xlink:href="#stroked-lock"/></svg>Cambiar contraseña</a></li>
        <li><a onclick="closeSession()"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg>Cerrar sesion</a></li>
        <li role="presentation" class="divider"></li>
        <li  class="active"><a href="downloadApp.php"><svg class="glyph stroked mobile device"><use xlink:href="#stroked-mobile-device"/></svg>Applicacion movil</a></li>
        <li><a href="https://www.paypal.me/konrad213"><svg class="glyph stroked bacon burger"><use xlink:href="#stroked-bacon-burger"/></svg>Invitame a una tapa!</a></li>
        <li><a onclick="alert('Para dudas, sugerencias, contacto o soporte escribeme a konrad213@hotmail.com');"><svg class="glyph stroked email"><use xlink:href="#stroked-email"/></svg>Sugerencia o contacto</a></li>
        <li><a href="basesLegalesLogin.php"><svg class="glyph stroked paperclip"><use xlink:href="#stroked-paperclip"/></svg>Bases legales</a></li>
    </ul>
</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Descarga la aplicación oficial de Mas tapas</div>
					<div class="panel-body">
                        <div class="col-lg-6">
                            <img style="max-height: 100%; max-width: 100%" src="img/splash.png">

                        </div>
                        <div class="col-lg-6">
                            <h1 class="text-center">Descarga la aplicación clickando <a href="https://play.google.com/apps/testing/com.konrad.mastapas" >aquí</a></h1>

                        </div>




					</div>
				</div>
			</div><!-- /.col-->
		</div><!-- /.row -->
		
	</div><!--/.main-->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script>
		!function ($) {
			$(document).on("click","ul.nav li.parent > a > span.icon", function(){
				$(this).find('em:first').toggleClass("glyphicon-minus");
			});
			$(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>
</body>

</html>
    <?php
} else {
    header("Location: index.php");
}

?>